# uno_fastio

uno_fastio.h is a header file providing pin-defines and macros of the uno_fastio module of the uno_fastio library. These macros allow easy manipulation
of digital I/O pins of Arduino Uno Rev.3 - compatible development boards.

## Using uno_fastio
To use the functions provided by the library uno_fastio, download uno_fastio and unzip it in a directory that is scanned for libraries by the linker of the compiler (or point the linker to the directory of the uno_fastio directory).
If you use PlatformIO, this is done by creating a C project and making sure that the following lines are present in the platformio.ini file of the project:
```
lib_deps =
  https://gitlab.com/itlernpfad_public/uno_fastio.git
```  

The following example illustrates how to use the uno_fastio library.
To be able to compile, you have to replace n with the desired Arduino digital IO pin number. Use Arduino pin numbers as written on the Arduino Uno development board. 
For the pins labeled A0 - A5, replace n by the number given in the table below:

| Arduino pin label | *n* |
|-------------------|-----|
| A0                | 14  |
| A1                | 15  |
| A2                | 16  |
| A3                | 17  |
| A4                | 18  |
| A5                | 19  |

In the following example, if you want to manipulate Arduino digital pin 13, replace DIO*n*_DDR with DIO13_DDR, replace DIO*n*_PORT with DIO13_PORT and DIO*n*_BIT with DIO13_BIT:
```c
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
 * uno_fastio_example2 (https://gitlab.com/itlernpfad_public/uno_fastio)
 * Copyright 2021 The uno_fastio Authors (https://gitlab.com/itlernpfad_public/uno_fastio/-/blob/master/AUTHORS)
 * Copyright 2021 ITlernpfad
 */

/* 
 * This listing showcases how to use the uno_fastio library to blink an LED connected to Arduino Uno digital I/O pin n. 
 * For functional software, n must be replaced by the actual Arduino Uno digital I/O pin number to which the LED is connected.
 * For example, to blink the built-in LED at Arduino Uno pin 13, replace n with 13.
 */

#include <avr/io.h>
#include <util/delay.h>
/* 
 * https://gitlab.com/-/ide/project/itlernpfad_public/uno_fastio/. 
 * Provide pin-defines and macros of the uno_fastio module of the uno_fastio library for manipulating pins of 
 * Arduino Uno Rev.3 - compatible development boards. 
 */
#include "uno_fastio.h" 

int main(void) {


  /* bit DIOn_BIT des Registers DIOn_DDR auf 1 setzen, um den Arduino Uno Digital I/O pin n als output zu konfigurieren */
  DIOn_DDR |= (1<<DIOn_BIT);

  for(;;) {
  
    /* bit DIOn_BIT des Registers DIOn_PORT auf 1 setzen, um Arduino Uno Digital I/O pin n auf HIGH zu stellen. Schaltet angeschlossene LED ein */
    DIOn_PORT |= (1 << DIOn_BIT);
    _delay_ms(100);

    /* bit DIOn_BIT des Registers DIOn_PORT klären, um Arduino Uno Digital I/O pin n auf LOW zu stellen. Schaltet  angeschlossene LED aus */
    DIOn_PORT &= ~(1 << DIOn_BIT)  
    _delay_ms(100);
  }

  return 0;
}

```

Below is the an example where the Arduino Uno digital IO pin 2 (connected to an external switch) is configured as input and Arduino Uno digital I/O pin 13 (with built-in LED) is configured as output: 
```c
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
 * uno_fastio_example2 (https://gitlab.com/itlernpfad_public/uno_fastio)
 * Copyright 2021 The uno_fastio Authors (https://gitlab.com/itlernpfad_public/uno_fastio/-/blob/master/AUTHORS)
 * Copyright 2021 ITlernpfad
 */

/* 
 * This software blinks the built-in LED at Arduino Uno digital I/O pin 13, if voltage between Arduino Uno pin 2 and GND pin is 5V 
 */

#include <avr/io.h>
#include <util/delay.h>

/* 
 * https://gitlab.com/itlernpfad_public/uno_fastio. 
 * Provides pin-defines and macros of the uno_fastio module of the uno_fastio library for manipulating pins of 
 * Arduino Uno Rev.3 - compatible development boards. 
 */
#include "uno_fastio.h" 

int main(void) {

  /* clear bit DIO2_BIT in register DIO2_DDR to configure Arduino Uno digital I/O pin 2 as input */
  DIO2_DDR &= ~(1 << DIO2_BIT);
  /* set bit DIO13_BIT in register DIO13_DDR to configure Arduino Uno Digital I/O pin 13 as output */
  DIO2_DDR |= (1 << DIO2_BIT);

  for(;;) {
    /* read bit DIO2_BIT of DIO2_RPORT (the AVR special function register PINx connected with Arduino Uno digital I/O pin 2) to read the status of Arduino Uno pin 2. */ 
    uint8_t inputPinState = (DIO2_RPORT >> DIO2_BIT) & 1
    if (inputPinState == 1) {
      /* state of Arduino Uno pin 2 is HIGH */

      /* set bit DIO2_BIT in register DIO2_PORT to 1 to drive Arduino Uno digital I/O pin 13 HIGH. This turns on the built-in LED. */
      DIO2_PORT |= (1 << DIO2_BIT);
      _delay_ms(100);

      /* clear bit 2 in register DIO2_PORT (write 0) to drive Arduino Uno digital I/O pin 13 LOW. This turns off the built-in LED. */
      DIO2_PORT &= ~(1 << DIO2_BIT)  
      _delay_ms(100);
  }

  return 0;
}

```
### Advanced examples

Below is another example which shows how to use the macros READ(IO) and WRITE(IO). 
```c
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
 * uno_fastio_example2 (https://gitlab.com/itlernpfad_public/uno_fastio)
 * Copyright 2021 The uno_fastio Authors (https://gitlab.com/itlernpfad_public/uno_fastio/-/blob/master/AUTHORS)
 * Copyright 2021 ITlernpfad
 */

/* 
 * This software blinks the built-in LED at Arduino Uno digital I/O pin 13, if voltage between Arduino Uno pin 2 and GND pin is 5V 
 */

#include <avr/io.h>
#include <util/delay.h>

/* 
 * https://gitlab.com/itlernpfad_public/uno_fastio. 
 * Provides pin-defines and macros of the uno_fastio module of the uno_fastio library for manipulating pins of 
 * Arduino Uno Rev.3 - compatible development boards. 
 */
#include "uno_fastio.h"

int main(void) {

  /* configure Arduino Uno digital I/O pin 2 as output */
  SETINPUT(2);


  /* configure Arduino Uno digital I/O pin 13 as output */
  SETOUTPUT(13);

  for(;;) {
    if (READ(2)) {
      /* state Arduino Uno digital pin 2 is HIGH */

      /* drive Arduino Uno digital I/O pin n HIGH. This turns the LED on */
      WRITE(13, 1);
      _delay_ms(1000);

      /* drive Arduino Uno digital I/O pin n LOW. This turns the LED off */
      WRITE(13, 0);
      _delay_ms(1000);
    }
  }
```

Using loops does not work in combination with macros SET_OUTPUT(IO), SET_INPUT(IO), WRITE(IO, V), or READ(IO). Below is an example how to use a loop to configure digital I/O pins:

```c
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
 * uno_fastio_example3 (https://gitlab.com/itlernpfad_public/uno_fastio)
 * Copyright 2021 The uno_fastio Authors (https://gitlab.com/itlernpfad_public/uno_fastio/-/blob/master/AUTHORS)
 * Copyright 2021 ITlernpfad
 */
 
 /* 
  * This software configures Arduino Uno digital I/O pins 8 to 12 as output
  */

#include <avr/io.h>
#include <util/delay.h>

/* 
 * https://gitlab.com/itlernpfad_public/uno_fastio. 
 * Provides pin-defines and macros of the uno_fastio module of the uno_fastio library for manipulating pins of 
 * Arduino Uno Rev.3 - compatible development boards. 
 */
#include "uno_fastio.h"

int main(void) {
  /* Arrays for configuration of digital I/O pins (See https://www.avrfreaks.net/forum/how-can-i-make-array-ports) */
  volatile unsigned char * const outputDataDirectionRegisters[] = {&DIO8_DDR, &DIO9_DDR, &DIO10_DDR, &DIO11_DDR, &DIO12_DDR}; 
  volatile unsigned char * const outputPortRegisters[] = {&DIO8_PORT, &DIO9_PORT, &DIO10_PORT, &DIO11_PORT, &DIO12_PORT};
  const unsigned char outputPinBits[] = {DIO8_BIT, DIO9_BIT, DIO10_BIT, DIO11_BIT, DIO12_BIT};
  const unsigned char outputCount = sizeof(ouputDataDirectionRegisters)/sizeof(ouputDataDirectionRegisters[0]);

  /* Configure Arduino Uno Digital I/O pins 8 to 12 as output pins */
  for (unsigned char i = 0; i < outputCount; i++) {
    unsigned char outputDDR = * outputDataDirectionRegisters[i];  // https://www.avrfreaks.net/forum/how-can-i-make-array-ports
    unsigned char outputPort = * outputDataDirectionRegisters[i];
    SETBIT(outputDDR, i);
    CLEARBIT(outputPort, i);

    //SET_OUTPUT(i);  // Compile error (DIOi_DDR and DIOi_PIN do not exist)
    //DIOi_DDR |= (1 << DIOi_BIT); // i in DIOi_DDR or DIOi_BIT is part of the name and not recognized as the variable i. DIOi_DDR and DIOi_BIT don't exists)  
    //DIO8_DDR |= (1 << DIO8_BIT);  // Not dependent on i
    //DIO8_DDR |= (1 << i);  // only works if all output pins belong to the same port of the Atmega328P (port B in this case). Better use DDRB |= (1 << i);
  }
  
  return 0;
}
```


## License information

```
uno_fastio (https://gitlab.com/itlernpfad_public/uno_fastio)
Copyright 2010 - 2021 The uno_fastio Authors (https://gitlab.com/itlernpfad_public/uno_fastio/-/blob/master/AUTHORS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


This software includes software developed by The uno_fastio Authors, see uno_fastio AUTHORS file
(https://gitlab.com/ITlernpfad_Public/uno_fastio/blob/master/AUTHORS).

Contributions by The uno_fastio Authors
Copyright 2010 - 2021 The uno_fastio Authors (https://gitlab.com/ITlernpfad_Public/uno_fastio/blob/master/AUTHORS).
Licensed under the terms of the GNU Lesser Public License, version 3, or any later 
version OR (at the user's discretion) under the terms of the GNU Lesser General License, 
version 3, or any later version. 
For details, see file LICENSES/LGPL-3.0-or-later.txt: 
https://gitlab.com/ITlernpfad_Public/uno_fastio/blob/master/LICENSES/LGPL-3.0-or-later.txt .


This software includes software developed by ITlernpfad:

Contributions by ITlernpfad
Copyright 2021 ITlernpfad
Licensed under the terms of the GNU Lesser Public License, version 3, or any later 
version. 
For details, see file LICENSES/LGPL-3.0-or-later.txt: 
https://gitlab.com/ITlernpfad_Public/uno_fastio/blob/master/LICENSES/LGPL-3.0-or-later.txt .


This software contains the uno_fastio-documentation.

uno_fastio-documentation
Copyright 2021 The uno_fastio Authors
Copyright 2021 ITlernpfad
The contents of the directory documentation/ are dual-licensed under the terms 
of the GNU Lesser General Public License, version 3 or any later version, OR 
(at the user's discretion) under the terms of the Apache-2.0 License. 
For details, see file documentation/LICENSE:
https://gitlab.com/ITlernpfad_Public/uno_fastio/blob/master/documentation/LICENSES/LGPL-3.0-or-later.txt .


This software contains the software fastio (fastio.h)

fastio (https://github.com/kliment/Sprinter/blob/b7b099bb0e0a8797b71abdf50e3da6880a86d967/Sprinter/fastio.h)
Copyright 2011 Kliment
Copyright 2010 - 2011 Triffid_Hunter
This code contibuted by Triffid_Hunter and modified by Kliment
Licensed under the terms of the GNU Lesser General Public License, version 3 or any later version.
For details, see file LICENSES/LGPL-3.0-or-later.txt:
https://gitlab.com/ITlernpfad_Public/uno_fastio/blob/master/LICENSES/LGPL-3.0-or-later.txt .


Portions of this software are inspired by parts of Atmel corporation (2004). 8-bit AVR Microcontroller. Application Note. AVR035: Efficient C Coding for AVR,
(https://www.microchip.com//wwwAppNotes/AppNotes.aspx?appnote=en590906)

Atmel corporation (2004). 8-bit AVR Microcontroller. Application Note. AVR035: Efficient C Coding for AVR,
https://www.microchip.com//wwwAppNotes/AppNotes.aspx?appnote=en590906 .
Copyright (c) Atmel Corporation 2003. All rights reserved.

```
