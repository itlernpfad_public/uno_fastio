// SPDX-License-Identifier: LGPL-3.0-or-later
/*
 * uno_fastio_example3 (https://gitlab.com/itlernpfad_public/uno_fastio)
 * Copyright 2021 The uno_fastio Authors (https://gitlab.com/itlernpfad_public/uno_fastio/-/blob/master/AUTHORS)
 * Copyright 2021 ITlernpfad
 */
 
 /* 
  * This software configures Arduino Uno digital I/O pins 8 to 12 as output and drives them LOW.
  */

#include <avr/io.h>
#include <util/delay.h>

/* 
 * https://gitlab.com/itlernpfad_public/uno_fastio. 
 * Provides pin-defines and macros of the uno_fastio module of the uno_fastio library for manipulating pins of 
 * Arduino Uno Rev.3 - compatible development boards. 
 */
#include "uno_fastio.h"

int main(void) {
  /* Arrays for configuration of digital I/O pins (See https://www.avrfreaks.net/forum/how-can-i-make-array-ports) */
  unsigned char volatile * const outputDataDirectionRegisters[] = {&DIO8_DDR, &DIO9_DDR, &DIO10_DDR, &DIO11_DDR, &DIO12_DDR}; 
  unsigned char volatile * const outputPortRegisters[] = {&DIO8_PORT, &DIO9_PORT, &DIO10_PORT, &DIO11_PORT, &DIO12_PORT};
  const unsigned char outputPinBits[] = {DIO8_BIT, DIO9_BIT, DIO10_BIT, DIO11_BIT, DIO12_BIT};
  const unsigned char outputCount = sizeof(ouputDataDirectionRegisters)/sizeof(ouputDataDirectionRegisters[0]);

  /* Configure Arduino Uno Digital I/O pins 8 to 12 as output pins and drive them LOW */
  for (unsigned char i = 0; i < outputCount; i++) {
    unsigned char outputDDR = * outputDataDirectionRegisters[i];  // https://www.avrfreaks.net/forum/how-can-i-make-array-ports
    unsigned char outputPort = * outputDataDirectionRegisters[i];
    SETBIT(outputDDR, i);  // configure Arduino Uno Digital I/O pin i as output pin
    CLEARBIT(outputPort, i);  // drive Arduino Uno Digital I/O pin i LOW

    //SET_OUTPUT(i);  // Compile error (DIOi_DDR and DIOi_PIN do not exist)
    //DIOi_DDR |= (1 << DIOi_BIT); // i in DIOi_DDR or DIOi_BIT is part of the name and not recognized as the variable i. DIOi_DDR and DIOi_BIT don't exists)  
    //DIO8_DDR |= (1 << DIO8_BIT);  // Not dependent on i
    //DIO8_DDR |= (1 << i);  // only works if all output pins belong to the same port of the Atmega328P (port B in this case). Better use DDRB |= (1 << i);
  }
  
  return 0;
}
