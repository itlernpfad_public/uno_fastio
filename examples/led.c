/* SPDX-License-Identifier: LGPL-3.0-or-later
 * 
 * uno_fastio (https://gitlab.com/itlernpfad_public/uno_fastio)
 * Copyright 2010 - 2021 The uno_fastio Authors (https://gitlab.com/itlernpfad_public/uno_fastio/-/blob/master/AUTHORS)
 * Copyright 2021 ITlernpfad
 * 
 * uno_fastio is based on the software fastio (fastio.h)
 * by Triffid_Hunter and Kliment, with modifications by ITlernpfad and other uno_fastio authors.
 * 
 * fastio (https://github.com/kliment/Sprinter/blob/b7b099bb0e0a8797b71abdf50e3da6880a86d967/Sprinter/fastio.h)
 * Copyright 2011 Kliment
 * Copyright 2010 - 2011 Triffid_Hunter
 * Licensed under the terms of the GNU General Public License, version 3 or any later version OR (at the user's discretion)
 * under the terms of the GNU Lesser General Public License, version 3
 * or any later version.
 */

/*
 * @package uno_fastio
 * @file examples/led.c 
 * @authors 2021 ITlernpfad and other uno_fastio authors
 * (https://gitlab.com/ITlernpfad_Public/uart/-/blob/master/AUTHORS)
 * @copyright 2021 The uno_fastio authors
 * Copyright 2021 ITlernpfad
 * @brief Part of uno_fastio
 * @details uno_fastio is a pin manipulation library for Arduino Uno-compatible development boards written in C.  
 * language: C99
 * status: Beta
 * @version 1.0.2
 */

/*
 * blink the built-in LED of the Arduino Uno development board Digital I/O pin 13. Pin 13 should be manipulated, so in
 * this example, n = 13.
 */

#include <avr/io.h>
#include <util/delay.h>
/* 
 * https://gitlab.com/itlernpfad_public/uno_fastio/.
 * Provide pin-defines and macros of the uno_fastio module of the uno_fastio library for manipulating pins of 
 * Arduino Uno Rev.3 - compatible development boards. 
 */
#include "uno_fastio.h" 

int main(void) {

  /*
   * With n = 2, set bit DIOn_BIT in DIOn_DDR to make Arduino Uno digital I/O pin n an output for DIOn_PORT. Alternatives:
   * SETBIT(DIO2_PORT, DIO2_BIT);
   * SET_INPUT(2)
   */
  DIO2_DDR &= ~(1U << DIO2_BIT);


  /*
   * With n = 13, set bit DIOn_BIT in DIOn_DDR to make Arduino Uno digital I/O pin n an output for DIOn_PORT. Alternatives:
   * SETBIT(DIO13_PORT, DIO13_BIT);
   * SET_OUTPUT(13)
   */
  DIO13_DDR |= (1U << DIO13_BIT);

  for(;;) {

    // if (bit_is_set(DIO2_RPORT, DIO2_BIT)) {


    /* read bit DIO2_BIT of DIO2_RPORT (the AVR special function register PINx connected with Arduino Uno digital I/O pin 2) to read the status of Arduino Uno pin 2. */ 
    uint8_t inputPinState = (DIO2_RPORT >> DIO2_BIT) & 1U
    
    if (inputPinState == HIGH) {  // Alternative: if (bit_is_set(DIO2_RPORT, DIO2_BIT)) {
      /* state of Arduino Uno pin 2 is HIGH */
      
      /* set bit DIOn_BIT in DIOn_PORT register to drive Arduino Uno digital I/O pin n HIGH. This turns the LED on */
      DIO13_PORT |= (1U << DIO13_BIT);
      _delay_ms(500);

      /* clear bit DIOn_BIT in DIOn_PORT register to drive Arduino Uno Digital I/O pin n LOW. This turns the LED off */
      DIO13_PORT &= ~(1U << DIO13_BIT);
      _delay_ms(500);
    }

    /* read bit DIO2_BIT of DIO2_RPORT (the AVR special function register PINx connected with Arduino Uno digital I/O pin 2) to read the status of Arduino Uno pin 2. */ 
    inputPinState = READBIT(DIO2_RPORT, DIO2_BIT)
    if (inputPinState == HIGH)

      /* set bit DIOn_BIT in DIOn_PORT register to drive Arduino Uno digital I/O pin n HIGH. This turns the LED on */
      SETBIT(DIO13_PORT, DIO13_BIT);
      _delay_ms(1000);

      /* clear bit DIOn_BIT in DIOn_PORT register to drive Arduino Uno Digital I/O pin n LOW. This turns the LED off */
      CLEARBIT(DIO13_PORT, DIO13_BIT);
      _delay_ms(1000);
    }

    if (READ(2)) {
      /* state Arduino Uno digital pin 2 is HIGH */

      /* drive Arduino Uno digital I/O pin n HIGH. This turns the LED on */
      WRITE(13, 1);
      _delay_ms(1000);

      /* drive Arduino Uno Digital I/O pin n LOW. This turns the LED off */
      WRITE(13, 0);
      _delay_ms(1000);
    }
  }

  return 0;
}
